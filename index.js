const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();


exports.onUserCreate = functions.firestore.document('Fecha_devolucion/{id_devolucion}').onCreate(async (snap, context) => {
    const values = snap.data();
    const query = db.collection("Fecha_devolucion");
    const snapshot = await query.where("id_devolucion", "==", values.id_devolucion).get();
    let devoluciones = 0;
    snapshot.forEach(querysnapshot => {
      devoluciones = querysnapshot.data().num_devoluciones
    });
    console.log(devoluciones);
    if (devoluciones >= 3) {
        try {
            await db.collection('Fecha_devolucion').doc(snap.id).delete();
        } catch (error) {
            console.log(error);
        }
    }
})